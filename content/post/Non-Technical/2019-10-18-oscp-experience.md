---
title: OSCP Experience
subtitle: My experience doing OSCP
date: 2019-10-18
tags: ["certs", "thoughts", "courses"]
categories: ["non-technical"]
draft: false
type: posts
---

# OSCP Experience

At the time of writing I just passed my OSCP and I thought I would follow the trend and make a blog post about my experience with both the exam and the course.

**Disclaimer:** this post is old. The OSCP has undergone many updates since I took it, please keep that in mind.

# PWK Experience

I originally was going to purchase 60 days however, in the end I decided to purchase 30 days. Which, I am glad I did because I certainly wouldn't have needed 60 days.

I spent around 6/30 days not inside the lab, so that left me with 24 days inside the lab, in that time I successfully rooted 30 boxes. I could've probably done more (and probably should have) but I didn't want to burn out.

Going into the labs, I already had a lot of experience on HTB. I had also completed various VulnHub machines prior. As well as doing the machines the OSCP like HTB boxes by TJNull. The difference in difficulty between HTB and PWK was something I did not expect. The PWK labs were considerably easier than HTB. Overall, I believe that the OSCP is a great entry-level cert. I expected it to be a little harder than it was. But it isn't so difficult that someone new to hackng couldn't buy 90 days and pass, they could.

# PWK Tips

- **Set a realistic timeframe for your self**.
- **Watch all of the videos & read the PDF**.
- **Read the writeup for the box Alpha so you know whats expected**.
- **If you buy 30 days, don't do the lab report, it's a waste of time**.
- **Don't read too many blog posts and scare yourself about the complexity**.

# Exam Experience

Before my exam, I decided instead of trying to last minute panic study (which is ineffective) I would just chill out. I was still at my computer, but all I did was "waste time". Watched YouTube, Twitch, shitposted. By the time my exam started at 20:00 I was hyped & ready to hack all the things.

I started the exam at 20:00 and by 20:30 I had already completed the buffer overflow, this gave me 25 points within the first 30 minutes of the exam starting, an, excellent confidence booster. Before I started the BOF, I had made sure all of my scans were already running, this meant that by the time the BOF was finished, I already had large portion of the data I needed ready to be absorbed.

I took a 15 minute break before making a start on the 20 pointer machine, within 1 hour I had a user shell however I got stuck for 2 hours after that trying to get root. So I took a small 15 minute break and when I came back I completed the 10 pointer machine in about 30 minutes. I took another 15 minute break after completing the 10 pointer and within less than 1 hour I got root on the 20 pointer machine I was previously stuck on. At this point I was just over 5 hours into the exam and I already had 55 points.

At this point I started the other 20 pointer which took roughly 2 hours for me to get a user shell on. I decided to take an hour break after which it took me another 3 hours to get the root shell on the box. At this point I had 75 points which was 5 more points than I needed to pass. I took a long 2 hour break and when I came back it took me 30 minutes to get a user shell on the final 25 pointer machine. This put me around 80-85 points, which is a pretty comfortable pass. I spent the next 4 hours banging my head into the wall trying to get a root shell on the final 25 pointer, but even with my Metasploit usage left I could not get it. At this point I was really tired and I already knew that I'd passed so I just decided to take a nap for 45 minutes and use the rest of my time available to start writing the report.

I worked on my report for the remainder of the time and ended my exam 30 minutes early once I knew I had everything I needed for the report. It took me around 3 more hours after my exam had finished to write the report. Then I took a nap for one hour. Once I woke back up, I proofed my report one final time before submitting it. Within 6 days, OffSec emailed me informing me I'd passed.


# Exam Tips

- **Document all the things**
- **Don't panic, you've got a lot of time**








