---
title: OSED Experience
date: 2021-05-18
tags: ["certs"]
categories: ["non-technical"]
draft: true
type: posts
---

At the time of writing, I am currently still working on my OSED. However, I wanted to start working on my thoughts and feelings towards it before I finished and then release the final post when I actually finish. This gives me more time to think critically about how I feel about the course, how I am doing, etc.

First thing I'd like to say, this change/update to the course is great. I'd heard a lot of good things about the OSCE but one thing I heard routinely was that even though some techniques were valid, a lot of the examples given were outdated, which led to some confusion and I guess frustration. This course has changed that. While there may still be some things which are a bit behind (its not cutting edge) this course has done a great job at porting the OSCE a lot clsoer to the current landscape. For that, I applaud Offensive Security.

# Expectations & Considerations

Before going into my experiences so far, I'd like to take some time to discuss things which I believe are quite important for those considering taking this course.

**This course is no joke**. This course isn't for the faint hearted. While the material does provide **a lot** of detail and guidance, this course is one of those things that requires extensive self-directed study invesment. You **need** to spend a lot of hours on this course and it's material to really understand it. You **need** to fail a bunch. I cannot stress this enough. I think if you purchase this course thinking that it will be another cert you can add to the collection, then you're destined to fail. Perhaps, if you're already very good at binary exploitation in Windows environments that might be the case bur for the majority of people, that isn't true.

Somewhat fitting in with my previous point but, if you don't **really** enjoy binary exploitation, I wouldn't bother taking this course. Again, it isn't easy. But most of all, it is a huge time investment and for the majority of people if you don't enjoy the content, the amount of time investment it takes to pass the exam will just not be worth it. I would make sure that you really enjoy this topic before you purchase the course because if you don't I think you're going to find yourself feeling frustrated and $1000 out of pocket...

If you have absolutely no assembly or programming experience, get some. There are a lot of sections in this course where reading and understanding assembly are vital. Equally, there are a lot of sections where thinking from the perspective of a developer is incredibly useful. I would recommend at the very least you take some x86 introduction course, [OpenSecurityTraining Intro x86 springs to mind]().

