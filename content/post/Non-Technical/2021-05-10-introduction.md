---
title: Introduction
subtitle: Purpose of this blog
date: 2021-05-10
tags: ["useless"]
categories: ["non-technical"]
draft: false
type: posts
---

The purpose of this blog is for any general hacking content such as CTF writeups, things I find interesting, etc.

Feel free to check out my other blogs in the **about** section, or use the links provided at the top (or bottom of the page)