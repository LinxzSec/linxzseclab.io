---
title: About me
comments: false
---

Hey, my name is Linxz. I am a vulnerability researcher who likes kernel exploitation and cryptography. I am also interested in CPUs and anything low-level. You can find my specific crypto blog below, as well some other useless information about me:

- https://crypto.linxz.tech/ (back soon)


If you need to contact me, you can do so via the following:

- **Keybase**: Linxz
- **Jabber**: Linxz@jabber.calyxinstitute.org
- **Matrix**: @linxz:matrix.org

Other accounts I operate:

- **GitHub**: [LinxzSec](https://github.com/LinxzSec/) (Inactive)
- **GitLab**: [LinxzSec](https://gitlab.com/LinxzSec)

**Anything not listed above does not belong to me!** 

If you would like a fast response then please reach out via Jabber/Matrix. For more professional conversations, please ask for my email via one of the aforementioned.